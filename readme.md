# React manual
using `node` `babel` `yarn` `webpack`

### Objectives
To understand the processes used by creat-react-app command to create a react app

### Project Status
`Done`

### Bitbucket link
`https://bitbucket.org/arunruet45/react-manual`

### Installation and Setup Instructions
You will need `git`, `node`  and  `yarn`  installed globally on your machine.

Open terminal and run these commands
```
git clone https://arunruet45@bitbucket.org/arunruet45/react-manual.git
cd react-manual
yarn install
```
To Run on local machine

`yarn start`

To Visit App on local

`https://localhost:[port-no]`

To build for production

`yarn build:prod`

### Core tech used by react 
```
React use 4 core things to create a react app using creat-react-app.
node, babel, npm/yarn and webpack 
here I tried to use those 4 things manually to creat a react app for 
better understanding of all those stuffs 
```

### Technical specification of used libraries

```
## react 
React library is responsible for creating views 

## react-dom
ReactDOM library is responsible to actually render UI in the browser.

## @babel/preset-react 
To convert jsx to js for browsers. 

## @babel/preset-env 
It is a smart preset that allows you to use the latest JavaScript 
without needing to micromanage which syntax transforms are needed 
by your target environment(s).

## @babel/core 
It contains the core functionality of Babel.

## babel-loader 
It is used by webpack to transpile Modern JS into the JS code that browsers can understand.
Since all browsers don’t understand javascript’s static class properties feature @babel/plugin-proposal-class-properties plugin 
transforms static class properties as well as properties declared with the property initializer syntax.

## html-webpack-plugin 
will use your custom index.html that will be rendered by webpack-dev-server
if you don’t pass any param in new HTMLWebpackPlugin() , then thehtml-webpack-plugin plugin will generate an HTML5 file for you that includes all your webpack bundles in the body using script tags.

## webpack
To create module bundles (combination and minification) for html, js, css files.

##webpack-cli
To run webpack commands (to run dev server, production build etc).

## webpack-dev-server
It will create a dev server and serve the app from this and update devlopment
changes accordingly. 

## style-loader, css-loader, 
webpack understands javascript so we need to convert the styles and images in javascript using these loaders
```